# Finance

A trading robot is a computer program that gets as input real time quotes from 
the stock markets, or foreign currency exchanges, and based on an algorithm
decides how to invest money, in general by means of opening very short-time 
positions (what it is called day trading).

Many of these robots are based on technical indicators, that is, metrics that
are derived from current and past prices (like moving averages, or 
resistance levels) to predict future prices.

It is an open question if it is possible to make any money using those robots,
that is, if they have a positive mathematical expectancy that is is enough
to cover the brokers commissions.

## Answer from the Theory of Nescience

We can study this problem with the aid of the theory of nescience. In order to
do that, we have randomly selected 40 trading robots over a period of 6 years,
and we have tested them (EUR/USD exchange over a period of one year - 2016,
in intervals of five minutes, and a spread of 2 pips).

In the next figure we can see the evolution of the nescience of these robots
along those 6 years:

[Nescience of Robots](https://gitlab.com/nescience/finance/blob/master/Robots_Forex.jpeg)

As we can see, nescience not only it does not decrease, but also, it increases.
So, our theory of nescience suggests that the subject of Forex trading robots
based on technical indicators can not be considered a scientific discipline.

For more information, please refer to my book [A Mathematical Theory of the Unknown](http://mathematicsunknown.com/)
